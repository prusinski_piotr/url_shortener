from django.db import models
import secrets


class Url(models.Model):
    url = models.CharField(max_length=250)
    shortcode = models.CharField(max_length=10, unique=True)

    def save(self, *args, **kwargs):
        token = secrets.token_urlsafe(5)
        self.shortcode = token
        super(Url, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.url)

