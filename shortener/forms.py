from django.forms import ModelForm
from .models import Url
# from .validators import validate_url, validate_dot_com
from django import forms

# Create the form class.
class UrlForm(ModelForm):
    class Meta:
        model = Url
        fields = ['url']

