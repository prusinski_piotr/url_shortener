from django.urls import reverse_lazy
from django.shortcuts import render, redirect
from django.views.generic import CreateView, RedirectView
from .forms import UrlForm
from .models import Url


class UrlCreate(CreateView):
    model = Url
    form_class = UrlForm
    success_url = reverse_lazy('url_success')

    def post(self, request):
        form = UrlForm(request.POST)
        context = {'form': form}
        template = "shortener/home.html"
        if form.is_valid():
            short_url = form.cleaned_data.get("url")
            obj, created = Url.objects.get_or_create(url=short_url)
            context = {
                "object": obj,
                "created": created
            }
            template = "shortener/url_success.html"
            return render(request, template, context)


def redirection_view(request, shortcode):
    url = Url.objects.get(shortcode=shortcode)
    response = redirect(url.url)
    return response





